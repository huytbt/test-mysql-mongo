const Sequelize = require('sequelize')

module.exports = {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  circuitID: {
    type: Sequelize.STRING
  },
  time: {
    type: Sequelize.INTEGER
  },
  longEnergy: {
    type: Sequelize.BIGINT
  },
  longReactiveEnergy: {
    type: Sequelize.BIGINT
  },
  voltageMin: {
    type: Sequelize.INTEGER
  },
  voltageMax: {
    type: Sequelize.INTEGER
  },
  currentMin: {
    type: Sequelize.INTEGER
  },
  currentMax: {
    type: Sequelize.INTEGER
  },
  cost: {
    type: Sequelize.DOUBLE
  },
  co2: {
    type: Sequelize.DOUBLE
  },
  price: {
    type: Sequelize.DOUBLE
  },
  usage_indicator: {
    type: Sequelize.ENUM('A', 'E', 'C'),
    allowNull: true,
    defaultValue: 'A'
  },
  tariff_id: {
    type: Sequelize.INTEGER
  },
  tariff_type: {
    type: Sequelize.STRING
  },
  created_at: {
    type: Sequelize.DATE
  },
  updated_at: {
    type: Sequelize.DATE
  }
}
