const mongoose = require('../services/Mongo')
const Schema = mongoose.Schema
module.exports = new Schema({
  circuitID: Number,
  time: Number,
  longEnergy: Number,
  longReactiveEnergy: Number,
  voltageMin: Number,
  voltageMax: Number,
  currentMin: Number,
  currentMax: Number,
  cost: Number,
  co2: Number,
  price: Number,
  usage_indicator: String,
  tariff_id: Number,
  tariff_type: String,
  created_at: Date,
  updated_at: Date
})
