const mongoose = require('mongoose')
const host = process.env.MONGO_HOST || '127.0.0.1'
const port = process.env.MONGO_PORT || 27017
const db = process.env.MONGO_DB || 'simblesense'
mongoose.connect(`mongodb://${host}:${port}/${db}`)
module.exports = mongoose