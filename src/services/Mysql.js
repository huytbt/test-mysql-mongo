const Sequelize = require('sequelize')
if (!global.sequelize) {
  global.sequelize = new Sequelize(process.env.MYSQL_DB || 'simblesense', process.env.MYSQL_USER || 'root', process.env.MYSQL_PASS || 'root', {
    host: process.env.MYSQL_HOST || '127.0.0.1',
    port: process.env.MYSQL_PORT || 3306,
    dialect: 'mysql',
    pool: {
      max: process.env.MYSQL_POOL_CONNECTION_MAX || 100,
      min: 0,
      acquire: 30000,
      idle: 30000
    },
    logging: false,
    operatorsAliases: false
  })
}
module.exports = global.sequelize
