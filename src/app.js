require('dotenv').config()

const b1Mongo = require('./b1-mongo-single-insert')
const b1Mysql = require('./b1-mysql-single-insert')
const b2Mongo = require('./b2-mongo-multiple-insert')
const b2Mysql = require('./b2-mysql-multiple-insert')

const express = require('express')
const app = express()
const port = 3001
const callback = (res, err) => {
  if (err) {
    return res.status(500).send(err.message)
  }
  res.send('ok')
}
app.post('/b1-mongo-single-insert', (req, res) => {
  b1Mongo(err => callback(res, err))
})
app.post('/b1-mysql-single-insert', (req, res) => {
  b1Mysql(err => callback(res, err))
})
app.post('/b2-mongo-multiple-insert', (req, res) => {
  if (!req.query.n) {
    return res.status(400).send('Query n is required.')
  }
  b2Mongo(req.query.n, err => callback(res, err))
})
app.post('/b2-mysql-multiple-insert', (req, res) => {
  if (!req.query.n) {
    return res.status(400).send('Query n is required.')
  }
  b2Mysql(req.query.n, err => callback(res, err))
})
app.listen(port, () => console.log(`App listening on port ${port}!`))
