const Mongo = require('./services/Mongo')
const modelSchema = require('./schemas/mongo')
const model = Mongo.model('energyTest', modelSchema)

const run = callback => {
  const instance = new model({
    circuitID: 1,
    time: Math.round((new Date()).getTime() / 1000),
    longEnergy: 123456789,
    longReactiveEnergy: 123123123
  })
  instance.save(err => {
    callback(err)
  })
}

module.exports = run
