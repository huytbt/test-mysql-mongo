const Mysql = require('./services/Mysql')
const modelSchema = require('./schemas/mysql')
const model = Mysql.define('energy_test', modelSchema, {
  tableName: 'energy_test',
  timestamps: true,
  createdAt: 'created_at',
  updatedAt: 'updated_at'
})

const run = callback => {
  Mysql.sync()
    .then(() => model.create({
      circuitID: 1,
      time: Math.round((new Date()).getTime() / 1000),
      longEnergy: 123456789,
      longReactiveEnergy: 123123123
    }))
    .then(jane => {
      return callback(null, jane.toJSON())
    })
    .catch(callback)
}

module.exports = run
