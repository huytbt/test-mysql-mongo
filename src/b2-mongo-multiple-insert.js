const async = require('async')
const Mongo = require('./services/Mongo')
const modelSchema = require('./schemas/mongo')
const model = Mongo.model('energyTest', modelSchema)

const run = (n, callback) => {
  const objects = []
  async.times(n, (i, cb) => {
    objects.push({
      circuitID: 1,
      time: Math.round((new Date()).getTime() / 1000),
      longEnergy: 123456789,
      longReactiveEnergy: 123123123
    })
    cb(null)
  }, () => {
    model.create(objects, err => {
      callback(err)
    })
  })
}

module.exports = run
