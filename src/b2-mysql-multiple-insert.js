const async = require('async')
const Mysql = require('./services/Mysql')
const modelSchema = require('./schemas/mysql')
const model = Mysql.define('energy_test', modelSchema, {
  tableName: 'energy_test',
  timestamps: true,
  createdAt: 'created_at',
  updatedAt: 'updated_at'
})

const run = (n, callback) => {
  const objects = []
  async.times(n, (i, cb) => {
    objects.push({
      circuitID: 1,
      time: Math.round((new Date()).getTime() / 1000),
      longEnergy: 123456789,
      longReactiveEnergy: 123123123
    })
    cb(null)
  }, () => {
    Mysql.sync()
      .then(() => model.bulkCreate(objects))
      .then(() => {
        return callback(null)
      })
      .catch(callback)
  })
}

module.exports = run
